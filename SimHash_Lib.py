from __future__ import division, unicode_literals
import re
import hashlib
import numbers
import collections

class ExtendedSimHash(object):

    def __init__(self, objects, thresh=2):

        self.threshold = thresh
        self.hash_length = 64
        count = len(objects)

        self.index_table = collections.defaultdict(set)

        for i, q in enumerate(objects):

            self.AddToDatabase(*q)

    index_table_size = lambda self: len(self.index_table)
    @property
    def BitShifts(self):
        return [self.hash_length // (self.threshold + 1) * z for z in range(self.threshold + 1)]

    def GetKeys(self, simhash):

        for i, child in enumerate(self.BitShifts):
            if i == (len(self.BitShifts) - 1):
                m = 2 ** (self.hash_length - child) - 1
            else:
                m = 2 ** (self.BitShifts[i + 1] - child) - 1
            c = simhash.value >> child & m
            yield '%x:%x' % (c, i)


    def NearDuplicates(self, doc_hash):

        assert doc_hash.hash_length == self.hash_length

        ans = set()

        for key in self.GetKeys(doc_hash):
            duplis = self.index_table[key]

            for duplicate in duplis:
                tempsim, obj_id = duplicate.split(',', 1)
                tempsim = SimHash(int(tempsim, 16))


                d = doc_hash.HammingDistance(tempsim)
                if d <= self.threshold:
                    ans.add(obj_id)
        return list(ans)

    def AddToDatabase(self, obj_id, simhash):

        assert simhash.hash_length == self.hash_length

        for key in self.GetKeys(simhash):
            v = '%x,%s' % (simhash.value, obj_id)
            self.index_table[key].add(v)




class SimHash(object):

    def __init__(self, value):
        self.hash_length = 64
        self.regexstring = r'[\w\u4e00-\u9fcc]+'
        self.value = None


        self.hashfunc = lambda x: int(hashlib.md5(x).hexdigest(), 16)

        if isinstance(value, SimHash):
            self.value = value.value

        elif isinstance(value, collections.Iterable):
            self.BuildFeatures(value)
        elif isinstance(value, numbers.Integral):
            self.value = value


    def BuildFeatures(self, features):
        v = [0] * self.hash_length
        masks = [1 << z for z in range(self.hash_length)]
        if isinstance(features, dict):
            features = features.items()
        for f in features:
            if isinstance(f, str):
                h = self.hashfunc(f.encode('utf-8'))
                w = 1

            for i in range(self.hash_length):
                v[i] += w if h & masks[i] else -w
        ans = 0
        for i in range(self.hash_length):
            if v[i] > 0:
                ans |= masks[i]
        self.value = ans

    def HammingDistance(self, another):
        assert self.hash_length == another.hash_length
        x = (self.value ^ another.value) & ((1 << self.hash_length) - 1)
        ans = 0
        while x:
            ans += 1
            x &= x - 1
        return ans

    def make_tokens(self, content):
        sliding_window = lambda content, width=4: [content[i:i + width] for i in range(max(len(content) - width + 1, 1))]
        content = content.lower()
        content = ''.join(re.findall(self.regexstring, content))
        ans = sliding_window(content)
        return ans


