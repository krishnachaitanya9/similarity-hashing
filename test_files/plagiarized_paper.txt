Notice of Violation of IEEE Publication Principles
“A Review and Research Towards Mobile Cloud Computing”
by Dipayan Dev, Krishna Lal Baishnab
in the Proceedings of the Second IEEE International Conference on Mobile Cloud Computing,
Services, and Engineering, April 2014, pp. 252-256
After careful and considered review of the content and authorship of this paper by a duly
constituted expert committee, this paper has been found to be in violation of IEEE’s Publication
Principles.
This paper duplicated original text and figures from the papers cited below. The original content
was copied without attribution (including appropriate references to the original author(s) and/or
paper title) and without permission.
Due to the nature of this violation, reasonable effort should be made to remove all past
references to this paper, and future references should be made to the following articles:
“A Survey of Mobile Cloud Computing: Architecture, Applications, and Approaches”
by Hoang T. Dinh, Chonho Lee, Dusit Niyato, and Ping Wang
in Wireless Communications and Mobile Computing, Vol 13, Issue 18, December 2013, pp.
1587–1611
“Mobile Cloud Computing: A Comparison of Application Models”
by Dejan Kovachev, Yiwei Cao and Ralf Klamma
in an ArXiv document available at http://arxiv.org/abs/1107.4940 July 2011
“Research on Mobile Cloud Computing: Review, Trend and Perspective”
by Han Qi, Abdullah Gani
in the Proceedings of the Second International Conference on Digital Information and
Communication Technology and its Applications (DICTAP), May 2012, pp. 195-202
A Review and Research towards Mobile
Cloud Computing
 Dipayan Dev1
 Krishna Lal Baishnab2
M.Tech, Computer Science and Engineering 1
 Assistant Professor 2
National Institute of Technology, Silchar1
 National Institute of Technology, Silchar2

dev.dipayan16@gmail.com1
 klbaishnab@gmail.com2
Abstract: With an explosive growth of the mobile
applications and emerging of cloud computing
concept, the Mobile Cloud Computing (MCC) has
become a potential technology for the mobile service
users. MCC integrates the technology of cloud
computing with mobile environment and has taken
part of major discussion thread in the IT world since
2009. The ABI Research predicts that the number of
mobile cloud computing subscribers is expected to
grow from 42.8 million (1.1% of total mobile users) in
2008 to 998 million (19% of total mobile users) in
2014. Despite of this hype achieved by mobile cloud
computing, the growth of mobile cloud computing
subscribers is still below expectations. According to
the recent survey conducted by the International Data
Corporation, most IT Executives and CEOs are not
interested in adopting such services due to the
drawbacks associated with this (e.g., battery life,
storage, and bandwidth), environment (e.g.,
heterogeneity, scalability, and availability), and
security (e.g., reliability and privacy). In spite of
various efforts to overcome these, there are a number
of loopholes and challenges that still exist in the
security policies of mobile cloud computing. This
paper will give a review of various challenges in this
field and measures to overcome such.
Keywords-Cloud computing; Mobile computing;
Virtualization; Offloading
I. INTRODUCTION
Mobile devices (e.g., Smartphone, tablet pcs,
etc) are increasingly becoming an essential part of
human life as the most effective and convenient
communication tools not bounded by time and
place. Mobile users accumulate rich experience of
various services from mobile applications, which
run on the devices and/or on remote servers via
wireless networks. The rapid progress of mobile
computing (MC) [1] becomes a powerful trend in
the development of IT technology as well as
commerce and industry fields. However, the mobile
devices are facing many challenges in their
resources (e.g., battery life, storage, and
bandwidth) and communications (e.g., mobility and
security) [2]. The limited resources significantly
impede the improvement of service qualities.
On the other hand, advances in the field of network
based computing and applications on demand have
led to an explosive growth of application models
such as cloud computing, software as a service,
community network, web store, and so on. As a
major application model in the era of the Internet,
the term, Cloud Computing (CC) has become a
significant research topic of the scientific and
industrial communities since 2007.
Commonly, cloud computing is described as a
range of services which are provided by an
Internet-based cluster system. Such cluster systems
consist of a group of low-cost servers or Personal
Computers (PCs), organizing the various resources
of the computers according to a certain
management strategy, and offering safe, reliable,
fast, convenient and transparent services such as
data storage, accessing and computing to clients.
According to the top ten strategic technology trends
for 2012 [3] provided by Gartner (a famous global
analytical and consulting company), cloud
computing has been on the top of the list.
So, with the explosion of mobile applications and
the taking support of CC for a variety of services
for mobile users, mobile cloud computing (MCC)
is introduced as an integration of cloud computing
into the mobile environment. Mobile cloud
computing brings new types of services and
facilities for mobile users to take full advantages of
cloud computing.
As the development of cloud computing is going
too fast, resources in mobile cloud computing
networks are virtualized and assigned in a group of
numerous distributed computers rather than in
traditional local computers or servers, and are
provided to mobile devices such as smartphones,
portable terminal, and so on.
Figure1. Mobile Cloud Computing
2014 2nd IEEE International Conference on Mobile Cloud Computing, Services, and Engineering
978-1-4799-4425-5/14 $31.00 © 2014 IEEE
DOI 10.1109/MobileCloud.2014.41
252
II. HOW MOBILE CLOUD COMPUTING
WORKS
A. Architecture for Mobile Applications in Cloud
Environment:
We start our MCC with an open source project for
mobile cloud platform called openmobster [4]. Its
architecture is as given in the Figure 2.
Figure 2: [4] The openmobster architecture for MCC
B. Typical services needed by a mobile cloud
client:
The most essential services include:
 Sync: This service synchronizes all state
changes made to the mobile or its
applications back with the Cloud Server.
 Push: It manages any state updates being
sent as a notification from the cloud
server. This improves the user’s
experience as it does not require the user
to pro-actively check for new information.
 OfflineApp: It is a service which carries
the management capabilities to create
smart coordination between low-level
services like Sync and Push. It frees the
programmer from the burden of writing
code to actually perform synchronization
as it is this service which decides
synchronization management and
mechanism which is best for the current
state. The moment the data channel for
any mobile application is established, all
synchronizations and push notifications
are automatically handled by OfflineApp
service.
 Network: It manages the communication
channel needed to receive Push
notifications from the server. It carries the
ability to establish proper connections
automatically. It is a very low-level
service and it shields any low-level
connection establishment, security
protocol details by providing a high level
interfacing framework.
 Database: It manages the local data
storage for the mo-bile applications.
Depending on the platform it uses the
corresponding storage facilities. It must
support storage among the various mobile
applications and must ensure thread safe
concurrent access. Just like Network
service it is also a low-level service.
 InterApp Bus: This service provides lowlevel
coordination/communication
between the suites of applications installed
on the device.
Figure 3 shows the client cloud stack.
Figure 3: [4] Client cloud stack
C. Typical services needed by a mobile cloud
server:
These are the essential services that must be
provided to the mobile apps by the server.
 Sync: Server Sync service synchronizes
device side App state changes with the
backend services where the data actually
originates. It also must provide a plug-in
framework to mobilize the backend data.
 Push: Server Push service monitors data
channels (from backend) for updates. The
moment updates are detected,
corresponding notifications are sent back
to the device. If the device is out of
coverage or disconnected for some reason,
it waits in a queue, and delivers the push
the moment the device connects back to
the network.
 Secure Socket-Based Data Service:
Depending on the security requirements of
the Apps this server side service must
provide plain socket server or a SSL-based
socket server or both.
 Security: Security component provides
authentication and authorization services
to make sure mobile devices connecting to
the Cloud Server are in fact allowed to
access the system. Every device must be
first securely provisioned with the system
before it can be used. After the device is
registered, it is challenged for proper
credentials when the device itself needs to
be activated. Once the device is activated,
all Cloud requests are properly
253
authenticated/authorized going.
 Management Console: Every instance of a
Cloud Server must have a Command Line
application such as the Management
Console as it provides user and device
provisioning functionalities. In the future,
this same component will have more
device management features like remote
data wipe, remote locking, remote
tracking, etc.
Figure 4 shows the mobile server cloud stack.
Figure 4: [4] Mobile server cloud stack
III. ADVANTAGE OF MOBILE CLOUD
COMPUTING
Cloud computing is known to be a promising
solution for mobile computing due to many reasons
(e.g., mobility, communication, and portability [5]).
In the following, it describes how the cloud can be
used to overcome obstacles in mobile computing,
thereby pointing out advantages of MCC.
A. Extending battery lifetime
 Battery is one of the main concerns for mobile
devices. Several solutions have been proposed to
enhance the CPU performance [6] and to manage
the disk an intelligent manner [7] to reduce power
consumption. However, these solutions require
changes in the structure of mobile devices, or they
require a new hardware that results in an increase
of cost and may not be feasible for all mobile
devices. Computation offloading technique is
proposed with the objective to migrate the large
computations and complex processing from
resource-limited devices (i.e., mobile devices) to
resourceful machines (i.e., servers in clouds). This
avoids taking a long application execution time on
mobile devices which results in large amount of
power consumption. [8] evaluates the effectiveness
of offloading techniques through several
experiments. The results demonstrate that the
remote application execution can save energy
significantly. In addition, many mobile applications
take advantages from task migration and remote
processing. For example, using memory arithmetic
unit and interface (MAUI) to migrate mobile game
components [9] to servers in the cloud can save
27% of energy consumption for computer games
and 45% for the chess game.
B. Improving data storage capacity and processing
power:
Storage capacity is also a constraint for mobile
devices. MCC is developed to enable mobile users
to store/access the large data on the cloud through
wireless networks. First example is the Amazon
Simple Storage Service (Amazon S3) [10] which
supports file storage service. Mobile photo sharing
service enables mobile users to upload images to
the clouds immediately after capturing. Users may
access all images from any devices. With cloud, the
users can save considerable amount of energy and
storage space on their mobile devices since all
images are sent and processed on the clouds.
Facebook is the most successful social network
application today, and it is also a typical example
of using cloud in sharing images. Cloud computing
can efficiently support various tasks for data
warehousing, managing and synchronizing multiple
documents online. Mobile applications also are not
constrained by storage capacity on the devices
because their data now is stored on the cloud).
C. Improving reliability
 Storing data or running applications on clouds is
an effective way to improve the reliability since the
data and application are stored and backed up on a
number of computers. This reduces the chance of
data and application lost on the mobile devices. In
addition, MCC can be designed as a comprehensive
data security model for both service providers and
users. For example, the cloud can be used to protect
copyrighted digital contents (e.g., video, clip, and
music) from being abused and unauthorized
distribution [11]. Also, the cloud can remotely
provide to mobile users with security services such
as virus scanning, malicious code detection, and
authentication [12]. Also, such cloud-based
security services can make efficient use of the
collected record from different users to improve the
effectiveness of the services.
In addition, MCC also inherits some advantages of
clouds for mobile services as follows:
a) Dynamic provisioning: Dynamic ondemand
provisioning of resources on a finegrained,
self-service basis is a flexible way for
service providers and mobile users to run their
applications without advanced reservation of
resources.
b) Scalability: The deployment of mobile
applications can be performed and scaled to meet
the unpredictable user demands due to flexible
254
resource provisioning. Service providers can easily
add and expand an application and service without
or with little constraint on the resource usage.
c) Multi-tenancy: Service providers (e.g.,
network operator and data centre owner) can share
the resources and costs to support a variety of
applications and large number of users.
d) Ease of Integration: Multiple services
from different service providers can be integrated
easily through the cloud and the Internet to meet
the user’s demands.
IV. CHALLENGES IN MOBILE CLOUD
COMPUTING
The main objective of mobile cloud computing
is to provide a convenient and rapid method for
users to access and receive data from the cloud,
such convenient and rapid method means accessing
cloud computing resources effectively by using
mobile devices. The major challenge of mobile
cloud computing comes from the characters of
mobile devices and wireless networks, as well as
their own restriction and limitation, and such
challenge makes application designing,
programming and deploying on mobile and
distributed devices more complicated than on the
fixed cloud device. In mobile cloud computing
environment, the limitations of mobile devices,
quality of wireless communication, types of
application, and support from cloud computing to
mobile are all important factors that affect
assessing from cloud computing. Table 1 gives an
overview of proposed challenges and some
solutions about mobile cloud computing.
A. Limitations of mobile devices
While discussing mobile devices in cloud the first
thing is resource-constrain. Though smartphones
have been improved obviously in various aspects
such as capability of CPU and memory, storage,
size of screen, wireless communication, sensing
technology, and operation systems, still have
serious limitations such as limited computing
capability and energy resource, to deploy
complicated applications. By contrast with PCs and
Laptops in a given condition, these smartphones
like iPhone 4S, Android serials, Windows Mobile
serials decrease 3 times in processing capacity, 8
times in memory, 5 to 10 times in storage capacity
and 10 times in network bandwidth. Normally,
smart phone needs to be charged everyday as
dialling calls, sending messages, surfing the
Internet, community accessing, and other internet
applications. According to past development
trends, the increased mobile computing ability and
rapid development of screen technology will lead
to more and more complicated applications
deployed in smartphones. If the battery technology
cannot be improved in a short time, then how to
effectively save battery power in smart phone is a
major issue we meet today. The processing
capacity, storage, battery time, and communication
of those smartphones should be improved
consistently with the development of mobile
computing. However, such enormous variations
will persist as one of major challenges in mobile
cloud computing.
TABLE I CHALLENGES AND SOLUTIONS OFMOBILE
CLOUD COMPUTING
B. Quality of communication
In contrast with wired network uses physical
connection to ensure bandwidth consistency, the
data transfer rate in mobile cloud computing
environment is constantly changing and the
connection is discontinuous due to the existing
clearance in network overlay. Furthermore, data
centre in large enterprise and resource in Internet
service provider normally is far away to end users,
especially to mobile device users. In wireless
network, the network latency delay may 200 ms in
’last mile’ but only 50 ms in traditional wired
network. Some other issues such as dynamic
changing of application throughput, mobility of
users, and even weather will lead to changes in
bandwidth and network overlay. Therefore, the
handover delay in mobile network is higher than in
wired network
C. Division of Application Services
In order to dynamically shift the computation
between mobile device and cloud, applications
needed to be split in loosely coupled modules
interacting with each other. The modules are
dynamically shifted between mobile devices and
cloud depending on the several metric parameters
modelled in cost model [15] .These parameters can
include the module execution time, resource
consumption, battery level, monetary costs,
security or network bandwidth. User waiting time,
which is a key aspect here i.e. the time a user, waits
from invoking some actions on the device’s
Challenges Solutions
Limitations of mobile devices
Improvement of processing
capacity, storage, battery
time of mobile devices
Quality of communication Bandwidth upgrading, Data
delivery time reducing
Division of applications
services
Fast optimization algorithm
techniques based on which
tasks to be shifted onto the
remote servers
255
interface until a desired output or exception are
returned to the user.
Figure 5: Cost model of elastic mobile cloud applications

Cost Model: The cost model takes inputs from both
device and cloud, and runs optimization algorithms
to decide execution configuration of applications
(Fig. 5). Zhang et al. [13] use Na¨ıve Bayesian
Learning classifiers to find the optimal execution
configuration from all possible configurations with
given CPU, memory and network consumption,
user preferences, and log data from the application.
Guirgiu et al. [14] model the application behaviour
through a resource consumption graph. Every
bundle or module composing the application has
memory consumption, generated input and output
traffic, and code size. Application’s distribution
between the server and phone is then optimized.
The server is assumed to have infinite resources
and the client has several resource constraints. The
partitioning problem seeks to find an optimal
solution in the graph satisfying an objective
function and device’s constraints. The objective
function tries to minimize the interactions between
the phone and the server, while taking into account
the overhead of acquiring and installing the
necessary bundles. However, optimization
involving many interrelated parameters in the cost
model can be time or computation consuming, and
even can override the cost savings. Therefore,
approximate and fast optimization techniques
involving prediction are needed. The model could
predict costs of different partitioning configurations
before running the application and deciding on the
best one.
V. CONCLUSION
Mobile cloud computing is one of major mobile
technology trends in the future since it combines
the advantages of both mobile computing and cloud
computing, thereby providing optimal services for
mobile users. The concept of cloud computing
provides a brand new opportunity for the
development of mobile applications since it allows
the mobile devices to maintain a very thin layer for
user applications and shift the computation and
processing overhead to the virtual environment.
This paper covered several representative mobile
cloud approaches. Much other related works exist,
but the purpose of this paper is to give an overview
of the wide spectrum of mobile cloud computing
possibilities. None of the existing approaches meets
completely the requirements of mobile clouds.
Mobile cloud computing will be a source of
challenging research problems in information and
communication technology for many years to
come. Solving these problems will require
interdisciplinary research from systems and
networks.
REFERENCES
[1] M. Satyanarayanan, “Mobile computing: the next decade,” in
Proceedings of the 1st ACM Workshop on Mobile Cloud
Computing & Services: Social Networks and Beyond (MCS),
June 2010.
[2] M. Satyanarayanan, “Fundamental challenges in mobile
computing,” in Proceedings of the 5th annual ACM symposium
on Principles of distributed computing, pp. 1-7, May 1996.
[3]M. Cooney. (2011, Oct) Gartner: The top 10 strategic
technology trends for 2012. [Online].
Available:http://www.networkworld.com/news/2011/101811-
gartner-technology-trends-252100.html
[4]https://code.google.com/p/openmobster/w/list [5]G. H.
Forman and J. Zahorjan,“The Challenges of Mobile
Computing,” IEEE Computer Society Magazine, April 1994.
[6]R. Kakerow, “Low power design methodologies for mobile
communication,” in proceedings of IEEE International
Conference on Computer Design: VLSI in Computers and
Processors, pp. 8, January 2003.
[7]R. N. Mayo and P. Ranganathan, “Energy Consumption in
Mobile Devices: Why Future systems Need Requirements
Aware Energy Scale-Down,” in Proceedings of the Workshop
on Power-Aware Computing Systems, October 2003.
[8]A. Rudenko, P. Reiher, G. J. Popek, and G. H. Kuenning,
“Saving portable computer battery power through remote
process execution,” Journal of ACM SIGMOBILE on Mobile
Computing and Communications Review, vol. 2, no. 1, January
1998.
[9]E. Cuervo, A. Balasubramanian, Dae-ki Cho, A. Wolman, S.
Saroiu, R. Chandra, and P. Bahl, “MAUI: Making Smartphones
Last Longer with Code offload,” in Proceedings of the 8th
International Conference on Mobile systems, applications, and
services, pp. 49-62, June 2010.
[10]http://aws.amazon.com/s3/
[11]P. Zou, C. Wang, Z. Liu, and D. Bao, “Phosphor: A Cloud
Based DRM Scheme with Sim Card,” in Proceedings of the
12th International Asia-Pacific on Web Conference (APWEB),
pp. 459, June 2010.
[12]J. Oberheide, K. Veeraraghavan, E. Cooke, J. Flinn, and F.
Jahanian. “Virtualized in-cloud security services for mobile
devices,” in Proceedings of the 1st Workshop on virtualization
in Mobile Computing (MobiVirt), pp. 31-35, June 2008.
[13]X. Zhang, S. Jeong, A. Kunjithapatham, and Simon Gibbs,
“Towards an Elastic Application Model for Augmenting
Computing Capabilities of Mobile Platforms,” in The Third
International ICST Conference on MOBILe Wireless
MiddleWARE, Operating Systems, and Applications, Chicago,
IL, USA, 2010.
[14]I. Giurgiu, O. Riva, D. Juric, I. Krivulev, and G. Alonso,
“Calling the Cloud: Enabling Mobile Phones as Interfaces to
Cloud Applications,” in
Proceedings of the 10th ACM/IFIP/USENIX International
Conference on Middleware (Middleware ’09). Urbana
Champaign, IL, USA: Springer, Nov. 2009, pp. 1–20.
[15] Mobile Cloud Computing: A Comparison of Application
Models.Dejan Kovachev, Yiwei Cao and Ralf Klamma
256
