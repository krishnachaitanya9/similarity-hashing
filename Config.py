test_file_1 = 'test_files/test-1'
test_file_2 = 'test_files/test-2'
test_file_3 = 'test_files/test-3'
test_file_4 = 'test_files/test-4'
test_file_5 = 'test_files/test-5'
test_file_6 = 'test_files/test-6'
test_file_7 = 'test_files/plg-1'
test_file_8 = 'test_files/plg-2'
test_file_9 = 'test_files/plg-3'
plagiarized_paper = 'test_files/plagiarized_paper.txt'
no_of_shingles = 1

############ Database Related Entries ##########

database_host = 'localhost'
db_username = 'root'
db_password  = 'rootpw'
db_name = 'MinHash'
database_tf_idf_table = 'TFIDF_table'
database_docs_list_table = 'Documents'
Docs_folder = 'docs_folder'