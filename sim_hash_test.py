import re
from SimHash_Lib import SimHash, ExtendedSimHash
import Config

'''
Basically get_features is a character N-Gram. Run any sentence in debug and you will find out
'''
def features(s):
    width = 3
    s = s.lower()
    s = re.sub(r'[^\w]+', '', s)
    return [s[i:i + width] for i in range(max(len(s) - width + 1, 1))]


file_1 = Config.test_file_1
file_2 = Config.test_file_2
file_3 = Config.test_file_3
file_4 = Config.test_file_4
file_5 = Config.test_file_5
file_6 = Config.test_file_6
file_7 = Config.test_file_7
file_8 = Config.test_file_8
file_9 = Config.test_file_9


data = {
    1: open(file_1, 'r').read(),
    2: open(file_2, 'r').read(),
    3: open(file_3, 'r').read(),
    4: open(file_4, 'r').read(),
    5: open(file_5, 'r').read(),
    6: open(file_6, 'r').read(),
    7: open(file_7, 'r').read(),
    8: open(file_8, 'r').read(),
    9: open(file_9, 'r').read()

}
objs = [(str(k), SimHash(features(v))) for k, v in data.items()]
index = ExtendedSimHash(objs, thresh=6)
s1 = SimHash(features(open(Config.plagiarized_paper, 'r').read()))
print(index.NearDuplicates(s1))

